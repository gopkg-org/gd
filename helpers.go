/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package gd

import "C"
import "errors"
import "unicode/utf16"
import "unsafe"

func newError(str *C.char) error {
	if str != nil {
		return errors.New(C.GoString(str))
	}
	return nil
}

func cBool(b bool) C.int {
	if b {
		return 1
	} else {
		return 0
	}
}

func newUShort(s string) *uShort {
	runes := utf16.Encode([]rune(s))
	shorts := make([]C.ushort, len(runes))
	for i, r := range runes {
		shorts[i] = C.ushort(r)
	}
	return &uShort{ptr: unsafe.Pointer(&shorts[0])}
}

type uShort struct{ ptr unsafe.Pointer }

func (c *uShort) Close()       { Free(c.ptr) }
func (c *uShort) C() *C.ushort { return (*C.ushort)(c.ptr) }

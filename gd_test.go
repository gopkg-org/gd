/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package gd_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/gd"
)

//const (
//	Transparency = 2130706432
//	Red          = 16711680
//	Green        = 65280
//	Blue         = 255
//	White        = 16777215
//	Black        = 0
//)

var (
	Transparency = gd.TrueColorAlpha(0, 0, 0, 255)
	Red          = gd.TrueColorAlpha(255, 0, 0, 0)
	Green        = gd.TrueColorAlpha(0, 255, 0, 0)
	Blue         = gd.TrueColorAlpha(0, 0, 255, 0)
	White        = gd.TrueColorAlpha(255, 255, 255, 0)
	Black        = gd.TrueColorAlpha(0, 0, 0, 0)
)

func TestImage(t *testing.T) {
	suite.Run(t, new(TestImageSuite))
}

type TestImageSuite struct {
	suite.Suite
	img *gd.Image
}

func (suite *TestImageSuite) SetupSuite() {
	suite.img = gd.CreateTrueColor(5, 1)
	suite.NotNil(suite.img)
}

func (suite *TestImageSuite) TearDownSuite() {
	suite.img.Close()
}

func (suite *TestImageSuite) TestIsTrueColor() {
	suite.Equal(true, suite.img.IsTrueColor())
}

func (suite *TestImageSuite) TestWidth() {
	suite.Equal(5, suite.img.Width())
}

func (suite *TestImageSuite) TestHeight() {
	suite.Equal(1, suite.img.Height())
}

func (suite *TestImageSuite) TestGetColorsTotal() {
	suite.Equal(0, suite.img.GetColorsTotal())
}

func (suite *TestImageSuite) TestGetTransparent() {
	suite.Equal(uint8(255), suite.img.GetTransparent())
}

func (suite *TestImageSuite) TestIsInterlaced() {
	suite.Equal(false, suite.img.IsInterlaced())
}

func (suite *TestImageSuite) TestGetResolutionX() {
	suite.Equal(uint(96), suite.img.GetResolutionX())
}

func (suite *TestImageSuite) TestGetResolutionY() {
	suite.Equal(uint(96), suite.img.GetResolutionY())
}

func (suite *TestImageSuite) TestSetPixel() {
	suite.img.SetPixel(0, 0, Red)
	suite.img.SetPixel(1, 0, Green)
	suite.img.SetPixel(2, 0, Blue)
	suite.img.SetPixel(3, 0, White)
	suite.img.SetPixel(4, 0, Black)

	suite.Equal(Red, suite.img.GetPalettePixel(0, 0))
	suite.Equal(Green, suite.img.GetPalettePixel(1, 0))
	suite.Equal(Blue, suite.img.GetPalettePixel(2, 0))
	suite.Equal(White, suite.img.GetPalettePixel(3, 0))
	suite.Equal(Black, suite.img.GetPalettePixel(4, 0))

	suite.Equal(Red, suite.img.GetTrueColorPixel(0, 0))
	suite.Equal(Green, suite.img.GetTrueColorPixel(1, 0))
	suite.Equal(Blue, suite.img.GetTrueColorPixel(2, 0))
	suite.Equal(White, suite.img.GetTrueColorPixel(3, 0))
	suite.Equal(Black, suite.img.GetTrueColorPixel(4, 0))
}

func TestPointF(t *testing.T) {
	suite.Run(t, new(TestPointFSuite))
}

type TestPointFSuite struct {
	suite.Suite

	val *gd.PointF
}

func (suite *TestPointFSuite) SetupSuite() {
	suite.val = gd.NewPointF(1, 2)
	suite.NotNil(suite.val)
}

func (suite *TestPointFSuite) TestX() {
	suite.Equal(float64(1), suite.val.X())
}

func (suite *TestPointFSuite) TestY() {
	suite.Equal(float64(2), suite.val.Y())
}

func TestPoint(t *testing.T) {
	suite.Run(t, new(TestPointSuite))
}

type TestPointSuite struct {
	suite.Suite

	val *gd.Point
}

func (suite *TestPointSuite) SetupSuite() {
	suite.val = gd.NewPoint(1, 2)
	suite.NotNil(suite.val)
}

func (suite *TestPointSuite) TestX() {
	suite.Equal(1, suite.val.X())
}

func (suite *TestPointSuite) TestY() {
	suite.Equal(2, suite.val.Y())
}

func TestRect(t *testing.T) {
	suite.Run(t, new(TestRectSuite))
}

type TestRectSuite struct {
	suite.Suite

	val *gd.Rect
}

func (suite *TestRectSuite) SetupSuite() {
	suite.val = gd.NewRect(1, 2, 3, 4)
	suite.NotNil(suite.val)
}

func (suite *TestRectSuite) TestX() {
	suite.Equal(1, suite.val.X())
}

func (suite *TestRectSuite) TestY() {
	suite.Equal(2, suite.val.Y())
}

func (suite *TestRectSuite) TestWidth() {
	suite.Equal(3, suite.val.Width())
}

func (suite *TestRectSuite) TestHeight() {
	suite.Equal(4, suite.val.Height())
}

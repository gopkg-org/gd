/*
 * Copyright (c) 2024, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package gd

/*
#include <gd.h>

#cgo CFLAGS: -I/usr/include
#cgo LDFLAGS: -lgd

// Avoid CGO bug https://github.com/golang/go/issues/19832
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
*/
import "C"
import "unsafe"

// Create - Creates a palette-based image (up to 256 colors).
func Create(width, height int) *Image {
	return newImage(C.gdImageCreate(C.int(width), C.int(height)))
}

// CreateTrueColor - Creates a true color image (millions of colors).
func CreateTrueColor(width, height int) *Image {
	return newImage(C.gdImageCreateTrueColor(C.int(width), C.int(height)))
}

func CreateFromPng(data []byte) *Image {
	return newImage(C.gdImageCreateFromPngPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromGif - Creates an image by decoding a GIF file from a byte slice.
func CreateFromGif(data []byte) *Image {
	return newImage(C.gdImageCreateFromGifPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromWBMP - Creates an image by decoding a WBMP file from a byte slice.
func CreateFromWBMP(data []byte) *Image {
	return newImage(C.gdImageCreateFromWBMPPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromJpeg - Creates an image by decoding a JPEG file from a byte slice.
func CreateFromJpeg(data []byte) *Image {
	return newImage(C.gdImageCreateFromJpegPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromJpegEx - Creates an extended image by decoding a JPEG file from a byte slice.
func CreateFromJpegEx(data []byte, ignoreWarning bool) *Image {
	return newImage(C.gdImageCreateFromJpegPtrEx(C.int(len(data)), unsafe.Pointer(&data[0]), cBool(ignoreWarning)))
}

// CreateFromWebp - Creates an image by decoding a WEBP file from a byte slice.
func CreateFromWebp(data []byte) *Image {
	return newImage(C.gdImageCreateFromWebpPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromHeif - Creates an image by decoding a HEIF file from a byte slice.
func CreateFromHeif(data []byte) *Image {
	return newImage(C.gdImageCreateFromHeifPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromAvif - Creates an image by decoding a AVIF file from a byte slice.
func CreateFromAvif(data []byte) *Image {
	return newImage(C.gdImageCreateFromAvifPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromTiff - Creates an image by decoding a TIFF file from a byte slice.
func CreateFromTiff(data []byte) *Image {
	return newImage(C.gdImageCreateFromTiffPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromTga - Creates an image by decoding a TGA file from a byte slice.
func CreateFromTga(data []byte) *Image {
	return newImage(C.gdImageCreateFromTgaPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromBmp - Creates an image by decoding a BMP file from a byte slice.
func CreateFromBmp(data []byte) *Image {
	return newImage(C.gdImageCreateFromBmpPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromGd - Creates an image by decoding a GD file from a byte slice.
func CreateFromGd(data []byte) *Image {
	return newImage(C.gdImageCreateFromGdPtr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromGd2 - Creates an image by decoding a GD2 file from a byte slice.
func CreateFromGd2(data []byte) *Image {
	return newImage(C.gdImageCreateFromGd2Ptr(C.int(len(data)), unsafe.Pointer(&data[0])))
}

// CreateFromGd2Part - Creates an part image by decoding a GD2 file from a byte slice.
func CreateFromGd2Part(data []byte, x, y, w, h int) *Image {
	return newImage(C.gdImageCreateFromGd2PartPtr(C.int(len(data)), unsafe.Pointer(&data[0]), C.int(x), C.int(y), C.int(w), C.int(h)))
}

func AlphaBlend(dst, src int) int { return (int)(C.gdAlphaBlend(C.int(dst), C.int(src))) }

func LayerOverlay(dst, src int) int { return (int)(C.gdLayerOverlay(C.int(dst), C.int(src))) }

func LayerMultiply(dst, src int) int { return (int)(C.gdLayerMultiply(C.int(dst), C.int(src))) }

func FontCacheSetup() bool { return (int)(C.gdFontCacheSetup()) != 0 }

func FontCacheShutdown() { C.gdFontCacheShutdown() }

func FreeFontCache() { C.gdFreeFontCache() }

func TrueColor(r, g, b uint8) int32 {
	return (int32(r) << 16) + (int32(g) << 8) + (int32(b) << 0)
}

// TrueColorAlpha - Compose a true color value from its components.
//
// Parameters:
//
//	r - The red channel (0-255)
//	g - The green channel (0-255)
//	b - The blue channel (0-255)
//	a - The alpha channel (0-127, where 127 is fully transparent, and 0 is completely opaque).
func TrueColorAlpha(r, g, b, a uint8) int32 {
	return int32(a)<<24 + (int32(r) << 16) + (int32(g) << 8) + (int32(b) << 0)
}

func Free(m unsafe.Pointer) { C.gdFree(m) }

func MajorVersion() int { return (int)(C.gdMajorVersion()) }

func MinorVersion() int { return (int)(C.gdMinorVersion()) }

func ReleaseVersion() int { return (int)(C.gdReleaseVersion()) }

func ExtraVersion() string { return C.GoString(C.gdExtraVersion()) }

func Version() string { return C.GoString(C.gdVersionString()) }

func newImage(img C.gdImagePtr) *Image {
	if img != nil {
		return &Image{ptr: img}
	}

	return nil
}

// Image - The data structure in which gd stores images. <gdImageCreate>, <gdImageCreateTrueColor> and the various
// image file-loading functions return a pointer to this type, and the other functions expect to receive a pointer
// to this type as their first argument.
type Image struct{ ptr C.gdImagePtr }

func (img *Image) Close() { C.gdImageDestroy(img.ptr) }

func (img *Image) IsTrueColor() bool { return (int)(img.ptr.trueColor) != 0 }

func (img *Image) Width() int { return (int)(img.ptr.sx) }

func (img *Image) Height() int { return (int)(img.ptr.sy) }

func (img *Image) GetColorsTotal() int { return (int)(img.ptr.colorsTotal) }

func (img *Image) GetTransparent() uint8 { return (uint8)(img.ptr.transparent) }

func (img *Image) IsInterlaced() bool { return (int)(img.ptr.interlace) != 0 }

func (img *Image) GetResolutionX() uint { return (uint)(img.ptr.res_x) }

func (img *Image) GetResolutionY() uint { return (uint)(img.ptr.res_y) }

func (img *Image) AlphaBlending(alphaBlending bool) {
	C.gdImageAlphaBlending(img.ptr, cBool(alphaBlending))
}

func (img *Image) SaveAlpha(saveAlpha bool) {
	C.gdImageSaveAlpha(img.ptr, cBool(saveAlpha))
}

func (img *Image) SetPixel(x, y int, color int32) {
	C.gdImageSetPixel(img.ptr, C.int(x), C.int(y), C.int(color))
}

func (img *Image) GetPalettePixel(x, y int) int32 {
	return (int32)(C.gdImageGetPixel(img.ptr, C.int(x), C.int(y)))
}

func (img *Image) GetTrueColorPixel(x, y int) int32 {
	return (int32)(C.gdImageGetTrueColorPixel(img.ptr, C.int(x), C.int(y)))
}

func (img *Image) AABlend() {
	C.gdImageAABlend(img.ptr)
}

func (img *Image) Line(x1, y1, x2, y2 int, color int32) {
	C.gdImageLine(img.ptr, C.int(x1), C.int(y1), C.int(x2), C.int(y2), C.int(color))
}

func (img *Image) DashedLine(x1, y1, x2, y2 int, color int32) {
	C.gdImageDashedLine(img.ptr, C.int(x1), C.int(y1), C.int(x2), C.int(y2), C.int(color))
}

func (img *Image) Rectangle(x1, y1, x2, y2 int, color int32) {
	C.gdImageRectangle(img.ptr, C.int(x1), C.int(y1), C.int(x2), C.int(y2), C.int(color))
}

func (img *Image) FilledRectangle(x1, y1, x2, y2 int, color int32) {
	C.gdImageFilledRectangle(img.ptr, C.int(x1), C.int(y1), C.int(x2), C.int(y2), C.int(color))
}

func (img *Image) SetClip(x1, y1, x2, y2 int) {
	C.gdImageSetClip(img.ptr, C.int(x1), C.int(y1), C.int(x2), C.int(y2))
}

func (img *Image) GetClip() (x1, y1, x2, y2 int) {
	var cx1, cy1, cx2, cy2 C.int
	C.gdImageGetClip(img.ptr, &cx1, &cy1, &cx2, &cy2)
	return int(cx1), int(cy1), int(cx2), int(cy2)
}

func (img *Image) SetResolution(x, y uint) {
	C.gdImageSetResolution(img.ptr, C.uint(x), C.uint(y))
}

func (img *Image) BoundsSafe(x, y int) int {
	return (int)(C.gdImageBoundsSafe(img.ptr, C.int(x), C.int(y)))
}

func (img *Image) Char(f Font, x, y, c int, color int32) {
	C.gdImageChar(img.ptr, f.ptr, C.int(x), C.int(y), C.int(c), C.int(color))
}

func (img *Image) CharUp(f Font, x, y, c int, color int32) {
	C.gdImageCharUp(img.ptr, f.ptr, C.int(x), C.int(y), C.int(c), C.int(color))
}

func (img *Image) String(f Font, x, y int, s string, color int32) {
	ps := unsafe.Pointer(C.CString(s))
	defer C.free(ps)
	C.gdImageString(img.ptr, f.ptr, C.int(x), C.int(y), (*C.uchar)(ps), C.int(color))
}

func (img *Image) StringUp(f Font, x, y int, s string, color int32) {
	ps := unsafe.Pointer(C.CString(s))
	defer C.free(ps)
	C.gdImageStringUp(img.ptr, f.ptr, C.int(x), C.int(y), (*C.uchar)(ps), C.int(color))
}

func (img *Image) String16(f Font, x, y int, s string, color int32) {
	ps := newUShort(s)
	defer ps.Close()
	C.gdImageString16(img.ptr, f.ptr, C.int(x), C.int(y), ps.C(), C.int(color))
}

func (img *Image) StringUp16(f Font, x, y int, s string, color int32) {
	ps := newUShort(s)
	defer ps.Close()
	C.gdImageStringUp16(img.ptr, f.ptr, C.int(x), C.int(y), ps.C(), C.int(color))
}

func (img *Image) StringTTF(fg int, fontList string, size, angle float64, x, y int, text string) ([]int, error) {
	cFontList := C.CString(fontList)
	defer C.free(unsafe.Pointer(cFontList))
	cText := C.CString(text)
	defer C.free(unsafe.Pointer(cText))
	brect := make([]C.int, 8)
	err := C.gdImageStringTTF(img.ptr, &brect[0], C.int(fg), cFontList, C.double(size), C.double(angle), C.int(x), C.int(y), cText)
	if err != nil {
		return nil, newError(err)
	}
	goBrect := make([]int, 8)
	for j, val := range brect {
		goBrect[j] = int(val)
	}
	return goBrect, nil
}

func (img *Image) StringFT(fg int, fontList string, size, angle float64, x, y int, text string) ([]int, error) {
	cFontList := C.CString(fontList)
	defer C.free(unsafe.Pointer(cFontList))
	cText := C.CString(text)
	defer C.free(unsafe.Pointer(cText))
	brect := make([]C.int, 8)
	err := C.gdImageStringFT(img.ptr, &brect[0], C.int(fg), cFontList, C.double(size), C.double(angle), C.int(x), C.int(y), cText)
	if err != nil {
		return nil, newError(err)
	}
	goBrect := make([]int, 8)
	for j, val := range brect {
		goBrect[j] = int(val)
	}
	return goBrect, nil
}

func (img *Image) Polygon(p *Point, n, c int) {
	C.gdImagePolygon(img.ptr, p.ptr, C.int(n), C.int(c))
}

func (img *Image) OpenPolygon(p *Point, n, c int) {
	C.gdImageOpenPolygon(img.ptr, p.ptr, C.int(n), C.int(c))
}

func (img *Image) FilledPolygon(p *Point, n, c int) {
	C.gdImageFilledPolygon(img.ptr, p.ptr, C.int(n), C.int(c))
}

func (img *Image) ColorAllocate(r, g, b uint8) int32 {
	return (int32)(C.gdImageColorAllocate(img.ptr, C.int(r), C.int(g), C.int(b)))
}

func (img *Image) ColorAllocateAlpha(r, g, b, a uint8) int32 {
	return (int32)(C.gdImageColorAllocateAlpha(img.ptr, C.int(r), C.int(g), C.int(b), C.int(a)))
}

func (img *Image) ColorClosest(r, g, b uint8) int32 {
	return (int32)(C.gdImageColorClosest(img.ptr, C.int(r), C.int(g), C.int(b)))
}

func (img *Image) ColorClosestAlpha(r, g, b, a uint8) int32 {
	return (int32)(C.gdImageColorClosestAlpha(img.ptr, C.int(r), C.int(g), C.int(b), C.int(a)))
}

func (img *Image) ColorClosestHWB(r, g, b uint8) int32 {
	return (int32)(C.gdImageColorClosestHWB(img.ptr, C.int(r), C.int(g), C.int(b)))
}

func (img *Image) ColorExact(r, g, b uint8) int32 {
	return (int32)(C.gdImageColorExact(img.ptr, C.int(r), C.int(g), C.int(b)))
}

func (img *Image) ColorExactAlpha(r, g, b, a uint8) int32 {
	return (int32)(C.gdImageColorExactAlpha(img.ptr, C.int(r), C.int(g), C.int(b), C.int(a)))
}

func (img *Image) ColorResolve(r, g, b uint8) int32 {
	return (int32)(C.gdImageColorResolve(img.ptr, C.int(r), C.int(g), C.int(b)))
}

func (img *Image) ColorResolveAlpha(r, g, b, a uint8) int32 {
	return (int32)(C.gdImageColorResolveAlpha(img.ptr, C.int(r), C.int(g), C.int(b), C.int(a)))
}

func (img *Image) ColorDeallocate(color int32) {
	C.gdImageColorDeallocate(img.ptr, C.int(color))
}

func (img *Image) CreatePaletteFromTrueColor(ditherFlag bool, colorsWanted int) *Image {
	return newImage(C.gdImageCreatePaletteFromTrueColor(img.ptr, cBool(ditherFlag), C.int(colorsWanted)))
}

func (img *Image) TrueColorToPalette(ditherFlag bool, colorsWanted int) int {
	return (int)(C.gdImageTrueColorToPalette(img.ptr, cBool(ditherFlag), C.int(colorsWanted)))
}

func (img *Image) PaletteToTrueColor() int {
	return (int)(C.gdImagePaletteToTrueColor(img.ptr))
}

func (img *Image) ColorMatch(dst *Image) bool {
	return (int)(C.gdImageColorMatch(dst.ptr, img.ptr)) == 1
}

func (img *Image) TrueColorToPaletteSetMethod(method, speed int) int {
	return (int)(C.gdImageTrueColorToPaletteSetMethod(img.ptr, C.int(method), C.int(speed)))
}

func (img *Image) TrueColorToPaletteSetQuality(minQuality, maxQuality int) {
	C.gdImageTrueColorToPaletteSetQuality(img.ptr, C.int(minQuality), C.int(maxQuality))
}

func (img *Image) ColorTransparent(color int32) {
	C.gdImageColorTransparent(img.ptr, C.int(color))
}

func (img *Image) PaletteCopy(dst *Image) {
	C.gdImagePaletteCopy(dst.ptr, img.ptr)
}

func (img *Image) ColorReplace(src, dst int32) int {
	return (int)(C.gdImageColorReplace(img.ptr, C.int(src), C.int(dst)))
}

func (img *Image) ColorReplaceThreshold(src, dst int32, threshold float64) int {
	return (int)(C.gdImageColorReplaceThreshold(img.ptr, C.int(src), C.int(dst), C.float(threshold)))
}

func (img *Image) ColorReplaceArray(src, dst []int32) int {
	if len(src) != len(dst) {
		panic("the src and dst slices must have the same length")
	}
	length := C.int(len(src))
	cSrc := make([]C.int, length)
	cDst := make([]C.int, length)
	for j, val := range src {
		cSrc[j] = C.int(val)
	}
	for j, val := range dst {
		cDst[j] = C.int(val)
	}
	return (int)(C.gdImageColorReplaceArray(img.ptr, length, &cSrc[0], &cDst[0]))
}

func (img *Image) FilledArc(cx, cy, w, h, s, e int, color int32, style int) {
	C.gdImageFilledArc(img.ptr, C.int(cx), C.int(cy), C.int(w), C.int(h), C.int(s), C.int(e), C.int(color), C.int(style))
}

func (img *Image) Arc(cx, cy, w, h, s, e int, color int32) {
	C.gdImageArc(img.ptr, C.int(cx), C.int(cy), C.int(w), C.int(h), C.int(s), C.int(e), C.int(color))
}

func (img *Image) Ellipse(cx, cy, w, h int, color int32) {
	C.gdImageEllipse(img.ptr, C.int(cx), C.int(cy), C.int(w), C.int(h), C.int(color))
}

func (img *Image) FilledEllipse(cx, cy, w, h int, color int32) {
	C.gdImageFilledEllipse(img.ptr, C.int(cx), C.int(cy), C.int(w), C.int(h), C.int(color))
}

func (img *Image) FillToBorder(x, y, border int, color int32) {
	C.gdImageFillToBorder(img.ptr, C.int(x), C.int(y), C.int(border), C.int(color))
}

func (img *Image) Fill(x, y int, color int32) {
	C.gdImageFill(img.ptr, C.int(x), C.int(y), C.int(color))
}

func (img *Image) Copy(dst *Image, dstX, dstY, srcX, srcY, w, h int) {
	C.gdImageCopy(dst.ptr, img.ptr, C.int(dstX), C.int(dstY), C.int(srcX), C.int(srcY), C.int(w), C.int(h))
}

func (img *Image) CopyMerge(dst *Image, dstX, dstY, srcX, srcY, w, h, pct int) {
	C.gdImageCopyMerge(dst.ptr, img.ptr, C.int(dstX), C.int(dstY), C.int(srcX), C.int(srcY), C.int(w), C.int(h), C.int(pct))
}

func (img *Image) CopyMergeGray(dst *Image, dstX, dstY, srcX, srcY, w, h, pct int) {
	C.gdImageCopyMergeGray(dst.ptr, img.ptr, C.int(dstX), C.int(dstY), C.int(srcX), C.int(srcY), C.int(w), C.int(h), C.int(pct))
}

func (img *Image) CopyResized(dst *Image, dstX, dstY, srcX, srcY, dstW, dstH, srcW, srcH int) {
	C.gdImageCopyResized(dst.ptr, img.ptr, C.int(dstX), C.int(dstY), C.int(srcX), C.int(srcY), C.int(dstW), C.int(dstH), C.int(srcW), C.int(srcH))
}

func (img *Image) CopyResampled(dst *Image, dstX, dstY, srcX, srcY, dstW, dstH, srcW, srcH int) {
	C.gdImageCopyResampled(dst.ptr, img.ptr, C.int(dstX), C.int(dstY), C.int(srcX), C.int(srcY), C.int(dstW), C.int(dstH), C.int(srcW), C.int(srcH))
}

func (img *Image) CopyRotated(dst *Image, dstX, dstY float64, srcX, srcY, srcW, srcH, angle int) {
	C.gdImageCopyRotated(dst.ptr, img.ptr, C.double(dstX), C.double(dstY), C.int(srcX), C.int(srcY), C.int(srcW), C.int(srcH), C.int(angle))
}

func (img *Image) Clone() *Image {
	return newImage(C.gdImageClone(img.ptr))
}

func (img *Image) ToTIFF() []byte {
	var size C.int
	data := C.gdImageTiffPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToBMP(compression int) []byte {
	var size C.int
	data := C.gdImageBmpPtr(img.ptr, &size, C.int(compression))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToWBMP(fg int) []byte {
	var size C.int
	data := C.gdImageWBMPPtr(img.ptr, &size, C.int(fg))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToJPEG(quality int) []byte {
	var size C.int
	data := C.gdImageJpegPtr(img.ptr, &size, C.int(quality))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToWEBP() []byte {
	var size C.int
	data := C.gdImageWebpPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToWEBPEx(quantization int) []byte {
	var size C.int
	data := C.gdImageWebpPtrEx(img.ptr, &size, C.int(quantization))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToHEIF() []byte {
	var size C.int
	data := C.gdImageHeifPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToHEIFEx(quality int, codec HeifCodec, chroma HeifChroma) []byte {
	cChroma := chroma.C()
	defer Free(unsafe.Pointer(cChroma))
	var size C.int
	data := C.gdImageHeifPtrEx(img.ptr, &size, C.int(quality), codec.C(), cChroma)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToAVIF() []byte {
	var size C.int
	data := C.gdImageAvifPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToAVIFEx(quality, speed int) []byte {
	var size C.int
	data := C.gdImageAvifPtrEx(img.ptr, &size, C.int(quality), C.int(speed))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToGIF() []byte {
	var size C.int
	data := C.gdImageGifPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToPNG() []byte {
	var size C.int
	data := C.gdImagePngPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToPNGEx(level int) []byte {
	var size C.int
	data := C.gdImagePngPtrEx(img.ptr, &size, C.int(level))
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToGD() []byte {
	var size C.int
	data := C.gdImageGdPtr(img.ptr, &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

func (img *Image) ToGD2(cs, fmt int) []byte {
	var size C.int
	data := C.gdImageGd2Ptr(img.ptr, C.int(cs), C.int(fmt), &size)
	defer Free(data)
	return C.GoBytes(data, size)
}

// NewPointF - Create a new PointF
func NewPointF(x, y float64) *PointF {
	var cPointF C.gdPointF
	cPointF.x, cPointF.y = C.double(x), C.double(y)
	return &PointF{ptr: &cPointF}
}

// PointF - Defines a point in a 2D coordinate system using floating point values.
type PointF struct{ ptr C.gdPointFPtr }

// X - Floating point position (increase from left to right).
func (p *PointF) X() float64 { return (float64)(p.ptr.x) }

// Y - Floating point Row position (increase from top to bottom).
func (p *PointF) Y() float64 { return (float64)(p.ptr.y) }

// NewPoint - Create a new Point
func NewPoint(x, y int) *Point {
	var cPoint C.gdPoint
	cPoint.x, cPoint.y = C.int(x), C.int(y)
	return &Point{ptr: &cPoint}
}

// Point - Represents a point in the coordinate space of the image; used by <gdImagePolygon>,
// <gdImageOpenPolygon> and <gdImageFilledPolygon> for polygon drawing.
type Point struct{ ptr C.gdPointPtr }

// X - Floating point position (increase from left to right).
func (p *Point) X() int { return (int)(p.ptr.x) }

// Y - Floating point Row position (increase from top to bottom).
func (p *Point) Y() int { return (int)(p.ptr.y) }

// NewRect - Create a new Rect
func NewRect(x, y, w, h int) *Rect {
	var cRect C.gdRect
	cRect.x, cRect.y, cRect.width, cRect.height = C.int(x), C.int(y), C.int(w), C.int(h)
	return &Rect{ptr: &cRect}
}

// Rect - A rectangle in the coordinate space of the image.
type Rect struct{ ptr C.gdRectPtr }

// X - The x-coordinate of the upper left corner.
func (r *Rect) X() int { return (int)(r.ptr.x) }

// Y - The y-coordinate of the upper left corner.
func (r *Rect) Y() int { return (int)(r.ptr.y) }

// Width - The width.
func (r *Rect) Width() int { return (int)(r.ptr.width) }

// Height - The height.
func (r *Rect) Height() int { return (int)(r.ptr.height) }

// Font - A font structure, containing the bitmaps of all characters in a font. Used to declare the characteristics
// of a font. Text-output functions expect these as their second argument, following the <gdImagePtr> argument.
// <gdFontGetSmall> and <gdFontGetLarge> both return one.
//
// You can provide your own font data by providing such a structure and the associated pixel array. You can
// determine the width and height of a single character in a font by examining the w and h members of the structure.
// If you will not be creating your own fonts, you will not need to concern yourself with the rest of the components
// of this structure.
type Font struct{ ptr C.gdFontPtr }

// TotalChars - # of characters in font
func (f *Font) TotalChars() int {
	return (int)(f.ptr.nchars)
}

// Offset - First character is numbered... (usually 32 = space)
func (f *Font) Offset() int {
	return (int)(f.ptr.offset)
}

// Width - Character width
func (f *Font) Width() int {
	return (int)(f.ptr.w)
}

// Height - Character height
func (f *Font) Height() int {
	return (int)(f.ptr.h)
}

// Data - Font data; array of characters, one row after another.
// Easily included in code, also easily loaded from data files.
func (f *Font) Data() []byte {
	p := f.ptr.data
	s := C.GoString(p)
	C.gdFree(unsafe.Pointer(p))
	return []byte(s)
}

// HeifCodec - HEIF Coding Format : Values that select the HEIF coding format.
type HeifCodec int

// C - Convert to type C.gdHeifCodec
func (v HeifCodec) C() C.gdHeifCodec {
	return (C.gdHeifCodec)(int(v))
}

const (
	HeifCodecUnknown HeifCodec = C.GD_HEIF_CODEC_UNKNOWN
	HeifCodecHEVC    HeifCodec = C.GD_HEIF_CODEC_HEVC
	HeifCodecAV1     HeifCodec = C.GD_HEIF_CODEC_AV1
)

// HeifChroma - HEIF Chroma Subsampling : Values that select the HEIF chroma subsampling.
type HeifChroma string

// C - Convert to type C.gdHeifChroma
func (v HeifChroma) C() C.gdHeifChroma {
	return (C.gdHeifChroma)(C.CString(string(v)))
}
